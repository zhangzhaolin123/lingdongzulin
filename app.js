App({
    d: {
      url: "https://lingdongyizu.heiwangke.com/index.php",
      imgurl:"https://lingdongyizu.heiwangke.com"
  },
  onLaunch(options) {
    my.getAuthCode({
    scopes: 'auth_user', // 主动授权（弹框）：auth_user，静默授权（不弹框）：auth_base
    success: (res) => {
    if (res.authCode) {
      // 认证成功
      // 调用自己的服务端接口，让服务端进行后端的授权认证，并且种session，需要解决跨域问题
      my.httpRequest({
        url:this.d.url+'api/index/denglu', // 该url是自己的服务地址，实现的功能是服务端拿到authcode去开放平台进行token验证
        data: {
          authcode: res.authCode
        },
        success: () => {
          // 授权成功并且服务器端登录成功
        },
        fail: () => {
          // 根据自己的业务场景来进行错误处理
        },
      });
    }
    },
});
  },
  onShow(options) {
    // 从后台被 scheme 重新打开
    // options.query == {number:1}
  },
});
