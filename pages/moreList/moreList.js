Page({
  data: {
    dataList: [0, 1, 2, 3, 4, 5, 6, 7],
    hasMoreData: true,
    isRefreshing: false,
    isLoadingMoreData: false
  },
  onLoad() { },
  onReachBottom() {
    if (this.data.isRefreshing || this.data.isLoadingMoreData || !this.data.hasMoreData) {
      return
    }
     this.setData({
        isLoadingMoreData: true,
      })
    setTimeout(() => {
      this.setData({
        dataList: this.data.dataList.concat([1, 2, 3, 4, 5]),
        isLoadingMoreData: false,
      })
      if (this.data.dataList.length > 30) {
        this.setData({
          hasMoreData: false
        })
      }
    }, 2000)

  }
});
