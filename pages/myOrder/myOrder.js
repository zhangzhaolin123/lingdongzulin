Page({
  data: {
     tabs: [
      { title: '全部'},
      { title: '待付款'},
      { title: '待发货' },
      { title: '待收货' },
      { title: '待归还' },
      { title:'完成' }
    ],
    activeTab: 0,
    dataList: [0, 1, 2, 3, 4, 5, 6, 7],
    hasMoreData: true,
    isRefreshing: false,
    isLoadingMoreData: false
  },
  onLoad(options) {
    this.setData({
      activeTab:Number(options.tabIndex)
    })
  },
  handleTabChange({ index }) {
    this.setData({
      activeTab: index,
      dataList: [0, 1, 2, 3, 4, 5, 6, 7],
      hasMoreData: true,
      isRefreshing: false,
      isLoadingMoreData: false
    });
    if (my.pageScrollTo) {
      my.pageScrollTo({
        scrollTop: 0
      })
    }
  },
   onReachBottom() {
    if (this.data.isRefreshing || this.data.isLoadingMoreData || !this.data.hasMoreData) {
      return
    }
     this.setData({
        isLoadingMoreData: true,
      })
    setTimeout(() => {
      this.setData({
        dataList: this.data.dataList.concat([1, 2, 3, 4, 5]),
        isLoadingMoreData: false,
      })
      if (this.data.dataList.length > 30) {
        this.setData({
          hasMoreData: false
        })
      }
    }, 2000)

  }
});
