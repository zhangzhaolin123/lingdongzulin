var app = getApp();
Page({
  data: {
    imgsbox: [],
    swiperCurrent: 0,
    autoplay: true,
    interval: 3000,
    duration: 800,
    // circular: true,
    // // 将轮播图组件自带的小点隐藏
    indicatorDots: false,
     dataList:[1,2,3,4,5,6],
  },
  
  onLoad() {
    var that=this;
    my.request({
    url: app.d.url + '/Api/Index/index',
    method: 'POST',
    data: {
    
      production: 'AlipayJSAPI',
    },
    dataType: 'json',
    success: function(res) {
      var lunbo=res.data.lunbo;
      var fenlei=res.data.fenlei;
      that.setData({
        lunbo:lunbo,
        fenlei:fenlei,
        imgurl:app.d.imgurl
      })
      
    },
    fail: function(res) {
      
    },
    complete: function(res) {
    
    }
  });
  },
  goodsname(e){
   console.log(e.detail.value);
   var that = this;
   var name=e.detail.value;
   that.setData({
      name:name
   })
  },
  sosuo(){
    var that = this;
    var name = that.data.name;
   my.navigateTo({
   url: '/pages/goodslist/goodslist?name='+name
  })

  }
});
