const goodsObj = require("../../mock/goodsObj.json")
Page({
  data: {
    imgs: ['../../images/dn.jpg', '../../images/dn.jpg', '../../images/dn.jpg'],
    indicatorDots: true,
    autoplay: false,
    interval: 3000,
    tabIndex: 0,
    showTop: false,
    goodsObj: {},
    modalOpened: false,//模态框
    order: {
      totalMoney: 100,
      params: ['白色', "4g", "12个月", "租完即送", "九成新"]
    },
    paramsObj:{
      
    },
    g_num:1
  },
  onLoad(options) {
    console.log(options)
    this.getData();
  },
  getData() {
    this.setData({
      goodsObj: goodsObj.data
    })
    // my.request({
    //   url: '../../mock/goodsObj.json',
    //   method: 'POST',
    //   data: {
    //     id: 1,
    //   },
    //   dataType: 'json',
    //   success: function (res) {
    //     my.alert({ content: 'success' });
    //   },
    //   fail: function (res) {
    //     my.alert({ content: 'fail' });
    //   },
    //   complete: function (res) {
    //     my.hideLoading();
    //   }
    // });
  },
  changeNum(e){
    let type = e.target.dataset.type;
    console.log(type)
    let num = this.data.g_num;
    if(type == "plus"){
      num++
    }else{
      if(this.data.g_num>1){
        num--
      }else{
        num = 1;
      }
    }
    this.setData({
        g_num:num
    })
  },
  inputNum(e){
    console.log(e)
    let obj = Number(e.detail.value);
    let num = 1;
    if(obj%1 !== 0){
      my.alert({
        content: "输入数字必须为整数"
      })
    }else if(obj<1){
      my.alert({
        content: "输入数字不能小于1"
      })
    }else{
      num= obj
    }
    this.setData({
        g_num:num
      })
  },
  // 加入购物车
  addShopCart(){

  },
  // 选择参数
  selectParam(e) {
    let m = e.target.dataset.m,
      index = e.target.dataset.i,
      params = this.data.goodsObj.goods_params[m].typeValue,
      paramskey = "goodsObj.goods_params["+m+"].typeValue";
    for (let i = 0; i < params.length; i++) {
      if (params[i].isActive) {
        let keyname = paramskey+"["+i+"].isActive";
        this.setData({
          [keyname]: false
        })
        break;
      }
    }
    let keyname = paramskey+"["+index+"].isActive"
    this.setData({
      [keyname]: true
    })
  },
  // 意外保障服务说明
  showExplain(){
    console.log(11)
    this.setData({
      modalOpened: true,
    });
  },
  onModalClose() {
    this.setData({
      modalOpened: false,
    });
  },
  // 商品说明，规格参数切换
  changeTab(e) {
    this.setData({
      tabIndex: e.target.dataset.index
    })
  },
  // 选择参数后确定
  paramOk() {
    
  },
  // 立即租赁
  onTopBtnTap() {
    this.setData({
      showTop: true,
    });
  },
  // 关闭参数的选择
  onPopupClose() {
    this.setData({
      showTop: false,
    });
  },
});
