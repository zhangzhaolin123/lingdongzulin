Page({
  data: {
     dataList: [0, 1, 2, 3, 4, 5, 6, 7],
    hasMoreData: true,
    isRefreshing: false,
    isLoadingMoreData: false,
    g_num:1
  },
  onLoad() {},
  changeNum(e){
    let type = e.target.dataset.type;
    console.log(type)
    let num = this.data.g_num;
    if(type == "plus"){
      num++
    }else{
      if(this.data.g_num>1){
        num--
      }else{
        num = 1;
      }
    }
    this.setData({
        g_num:num
    })
  },
  inputNum(e){
    console.log(e)
    let obj = Number(e.detail.value);
    let num = 1;
    if(obj%1 !== 0){
      my.alert({
        content: "输入数字必须为整数"
      })
    }else if(obj<1){
      my.alert({
        content: "输入数字不能小于1"
      })
    }else{
      num= obj
    }
    this.setData({
        g_num:num
      })
  },
  onReachBottom() {
    if (this.data.isRefreshing || this.data.isLoadingMoreData || !this.data.hasMoreData) {
      return
    }
     this.setData({
        isLoadingMoreData: true,
      })
    setTimeout(() => {
      this.setData({
        dataList: this.data.dataList.concat([1, 2, 3, 4, 5]),
        isLoadingMoreData: false,
      })
      if (this.data.dataList.length > 30) {
        this.setData({
          hasMoreData: false
        })
      }
    }, 2000)

  }
});
